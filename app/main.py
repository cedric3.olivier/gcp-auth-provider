import os
import re
from loguru import logger

from fastapi import FastAPI, HTTPException, Query
from fastapi.responses import PlainTextResponse

from app.gcp_client import get_iam_credentials, get_sts_token

app = FastAPI()


def guess_env_type() -> str:
    ref_name = os.getenv("CI_COMMIT_REF_NAME", "-")
    prod_ref = os.getenv("PROD_REF", "/^(master|main)$/").strip("/")
    if re.match(prod_ref, ref_name):
        # could be staging or prod
        if os.getenv("CI_JOB_STAGE", "-") in [
            "publish",
            "infra-prod",
            "production",
            ".post",
        ]:
            return "production"
        return "staging"

    integ_ref = os.getenv("INTEG_REF", "/^develop$/").strip("/")
    if re.match(integ_ref, ref_name):
        return "integration"

    return "review"


def get_var_prefix(env_type: str) -> str:
    if env_type == "review":
        return "REVIEW"
    if env_type == "integ" or env_type == "integration":
        return "INTEG"
    if env_type == "staging":
        return "STAGING"
    if env_type == "prod" or env_type == "production":
        return "PROD"
    raise HTTPException(
        status_code=404, detail=f"Unsupported environment type '{env_type}'"
    )


def get_oidc_account(var_prefix: str) -> str:
    return os.getenv(f"GCP_{var_prefix}_OIDC_ACCOUNT") or os.getenv("GCP_OIDC_ACCOUNT")


def get_oidc_provider(var_prefix: str) -> str:
    return os.getenv(f"GCP_{var_prefix}_OIDC_PROVIDER") or os.getenv(
        "GCP_OIDC_PROVIDER"
    )


@app.get("/ping", response_class=PlainTextResponse)
def ping():
    return "pong"


@app.get("/token")
def token(
    workload_identity_provider: str = Query(
        default=None, alias="workloadIdentityProvider"
    ),
    service_account: str = Query(default=None, alias="serviceAccount"),
    env_type: str = Query(default=None, alias="envType"),
):
    # projects/%s/locations/global/workloadIdentityPools/%s/providers/%s
    if (not workload_identity_provider) or (not service_account):
        # retrieve from TBC standard variables
        print("gcp-auth-provider: retrieve from TBC standard variables")
        logger.info("gcp-auth-provider: retrieve from TBC standard variables")
        if env_type is None:
            env_type = guess_env_type()

        var_prefix = get_var_prefix(env_type)

        workload_identity_provider = get_oidc_provider(var_prefix)
        service_account = get_oidc_account(var_prefix)
        if (not workload_identity_provider) or (not service_account):
            raise HTTPException(
                status_code=400,
                detail=f"Couldn't retrieve implicit OIDC provider/account from env for '{env_type}'",
            )

    audience = "//iam.googleapis.com/%s" % workload_identity_provider
    logger.info(f"gcp-auth-provider: get sts token {audience}")
    federated_token = get_sts_token(audience)
    logger.info(f"gcp-auth-provider: get_iam_credentials {service_account}")
    gcloud_auth_token = get_iam_credentials(service_account, federated_token)

    return gcloud_auth_token
