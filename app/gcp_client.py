import requests, json, os
from fastapi import HTTPException

CI_JOB_JWT_V2 = os.environ.get('CI_JOB_JWT_V2')


def get_iam_credentials(service_account, federated_token):
    resp = requests.request(
        method='POST',
        url='https://iamcredentials.googleapis.com/v1/projects/-/serviceAccounts/%s:generateAccessToken' % service_account,
        headers={
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer %s' % federated_token
        },
        data=json.dumps({
            "scope": ["https://www.googleapis.com/auth/cloud-platform"]
        })
    )
    if resp.status_code != 200:
        raise HTTPException(
            status_code=500,
            detail=f'Failed to get iam credential token: {resp.text}'
        )
    return resp.json()['accessToken']


def get_sts_token(audience):
    if CI_JOB_JWT_V2 is None:
        raise HTTPException(
            status_code=401,
            detail='Missing $CI_JOB_JWT_V2 token'
        )

    resp = requests.request(
        method='POST',
        url='https://sts.googleapis.com/v1/token',
        headers={
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        data=json.dumps({
            "audience": audience,
            "grantType": "urn:ietf:params:oauth:grant-type:token-exchange",
            "requestedTokenType": "urn:ietf:params:oauth:token-type:access_token",
            "scope": "https://www.googleapis.com/auth/cloud-platform",
            "subjectTokenType": "urn:ietf:params:oauth:token-type:jwt",
            "subjectToken": CI_JOB_JWT_V2
        })
    )
    if resp.status_code != 200:
        raise HTTPException(
            status_code=500,
            detail=f'Failed to get sts token: {resp.text}'
        )
    return resp.json()['access_token']
