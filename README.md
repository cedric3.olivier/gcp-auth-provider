# GCP Auth Provider

This project builds a Docker image with an API able to retrieve an access token for a given project using google apis.


It is aimed at being used in GitLab CI as a [service container](https://docs.gitlab.com/ee/ci/services/)
in order to decouple the image of your jobs and the way of retrieving a [GCP OAuth access token](https://developers.google.com/identity/protocols/oauth2).

## How to use

### API Overview

The service exposes one single API to retrieve a google oauth access token.

### GET token endpoint

Retrieve authentication token using API.

1. with **explicit** Service Account and Workload Identity Provider:
    ```
    GET /token?serviceAccount={serviceAccount}&workloadIdentityProvider={workloadIdentityProvider}
    ```
2. with **implicit** Service Account and Workload Identity Provider retrieved from to-be-continuous standard variables for an **explicitly** specified environment (_production_ here):
    ```
    GET /token?envType=prod
    ```
3. with **implicit** Service Account and Workload Identity Provider retrieved from to-be-continuous standard variables for **implicitly** guessed current environment):
    ```
    GET /token
    ```

#### Query Parameters


| Name                       | description                                                                               | required              | 
|----------------------------|-------------------------------------------------------------------------------------------|-----------------------|
| `serviceAccount`           | Default Service Account to which impersonate with OpenID Connect authentication           | `false` |
| `workloadIdentityProvider` | Default Workload Identity Provider associated with GitLab to [authenticate with OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/google_cloud/) | `false` |
| `envType`                  | The to-be-continuous environment type to retrieve associated Service Account and Workload Identity Provider values (one of `review`, `integration`, `staging` or `production`).<br/>_Only when `serviceAccount` and `workloadIdentityProvider` are not specified_ | `false` |

##### How are implicitly determined `serviceAccount` and `workloadIdentityProvider` values?

When not explicitly set, `serviceAccount` and `workloadIdentityProvider` values are retrieved from to-be-continuous standard variables for the associated `envType` (`envType` itself may also be guessed, see next chapter):

| `envType`        | `serviceAccount` value | `workloadIdentityProvider` value |
| ---------------- | ---------------------- | -------------------------------- |
| `production`     | `$GCP_PROD_OIDC_ACCOUNT` or `$GCP_OIDC_ACCOUNT` (fallback) | `$GCP_PROD_OIDC_PROVIDER` or `$GCP_OIDC_PROVIDER` (fallback) |
| `staging`        | `$GCP_STAGING_OIDC_ACCOUNT` or `$GCP_OIDC_ACCOUNT` (fallback) | `$GCP_STAGING_OIDC_PROVIDER` or `$GCP_OIDC_PROVIDER` (fallback) |
| `integration`    | `$GCP_INTEG_OIDC_ACCOUNT` or `$GCP_OIDC_ACCOUNT` (fallback) | `$GCP_INTEG_OIDC_PROVIDER` or `$GCP_OIDC_PROVIDER` (fallback) |
| `review`         | `$GCP_REVIEW_OIDC_ACCOUNT` or `$GCP_OIDC_ACCOUNT` (fallback) | `$GCP_REVIEW_OIDC_PROVIDER` or `$GCP_OIDC_PROVIDER` (fallback) |

##### How is guessed `envType`?

When not explicitly set, `envType` is automatically guessed based on [GitLab predefined variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html):

| `$CI_COMMIT_REF_NAME` | `envType` value |
| --------------------- | --------------- |
| `master` or `main`    | **production** if `$CI_JOB_STAGE` is one of `publish`, `infra-prod`, `production`, `.post`<br/>**staging** otherwise |
| `develop`             | **integration** |
| _any other branch_    | **review** |

#### Example

Let's suppose that you already have a service account and a workload identity provider configured on a gcp project

```bash
service_account={account_name}@{gcp_project_id}.iam.gserviceaccount.com
workload_identity_provider=projects/{gcp_project_number}/locations/global/workloadIdentityPools/{pool_id}/providers/{provider_id}
```

Then you may retrieve:

the token calling GET http://gcp-auth-provider/token?serviceAccount={service_account}&workloadIdentityProvider={workload_identity_provider}


## Use in Gitlab CI

Finally, the Docker image can be used in your GitLab CI files as follows:


```yaml
variables:
  CI_JOB_JWT_V2: $CI_JOB_JWT_V2
  GCP_OIDC_PROVIDER: "your-oidc-identity-provider-here"
  GCP_OIDC_ACCOUNT: "your-oidc-service-account-here"

deploy-job:
  image: my-deploy-tool:latest
  services:
    # add GCP Auth Provider as a service
    - name: $CI_REGISTRY/to-be-continuous/tools/gcp-auth-provider:main
      alias: gcp-auth-provider
  before-script:
    # retrieve some token from Vault server
    - my_token=$(curl -s -S -f "http://gcp-auth-provider/token")
    # then login
    - my-deploy-tool login --token $my_token
  script:
    # deploy (pseudo code)
    - my-deploy-tool deploy --other --args
```