FROM python:3.11 as requirements-stage
 
WORKDIR /tmp

# hadolint ignore=DL3013
RUN pip install --no-cache-dir poetry

COPY ./pyproject.toml ./poetry.lock* /tmp/

RUN poetry export -f requirements.txt --output requirements.txt --without-hashes

###########

FROM python:3.11-slim-buster

ENV PORT=80
WORKDIR /code
 
COPY --from=requirements-stage /tmp/requirements.txt /code/requirements.txt

RUN apt-get -y update && apt-get -y upgrade \
    &&  rm -rf /var/lib/apt/lists/* \
    && pip install --no-cache-dir --upgrade -r /code/requirements.txt

COPY ./app /code/app

EXPOSE ${PORT}
# hadolint ignore=DL3025
CMD uvicorn app.main:app --host=0.0.0.0 --port=${PORT}
